<?php
	$teamsarray = array(
			array("team_id" => "1", "team_name" => "Futbol Club Barcelona", "stadium" => "Camp Nou", "badge" => "fcbarcelona_badge.png", "strips" => "fcbarcelona_strips.png", "city_id" => "1", "dataapi_id" => "81", "info" => "Futbol Club Barcelona, also known as Barcelona and familiarly as Barça, is a professional football club, based in Barcelona, Catalonia, Spain.",
				"images" => array(
					array("image_id" => "67", "description" => "Camp Nou", "url" => "CampNou (Custom).jpg"),
					array("image_id" => "68", "description" => "Barcelona Current Squad", "url" => "Barcelona_Current_Squad (Custom).png")
				)
			),	
			array("team_id" => "2", "team_name" => "Reial Club Deportiu Espanyol de Barcelona SAD", "stadium" => "RCDE", "badge" => "espanyol_badge.png", "strips" => "espanyol_strips.png", "city_id" => "1", "dataapi_id" => "80", "info" => "Reial Club Deportiu Espanyol de Barcelona, commonly known as RCD Espanyol, or simply as Espanyol, is a professional sports club based in Barcelona, Catalonia, Spain.",
				"images" => array(
					array("image_id" => "69", "description" => "Estadio RCDE", "url" => "EstadioRCDE_Pano (Custom).jpg"),
					array("image_id" => "70", "description" => "Espanyol Current Squad", "url" => "Espanyol_Current_Squad (Custom).png")
				)
			),
			array("team_id" => "3", "team_name" => "Real Madrid Club de Futbol", "stadium" => "Santiago Bernabeu", "badge" => "realmadrid_badge.png", "strips" => "realmadrid_strips.png", "city_id" => "2", "dataapi_id" => "86", "info" => "Real Madrid Club de Fútbol, commonly known as Real Madrid, or simply as Real, is a professional football club based in Madrid, Spain. Founded in 1902 as Madrid Football Club, the team has traditionally worn a white home kit since inception.",
				"images" => array(
					array("image_id" => "71", "description" => "Santiago Bernabeu", "url" => "The_Santiago_Bernabeu_Stadium (Custom).jpg"),
					array("image_id" => "72", "description" => "Real Madrid Current Squad", "url" => "RealMadrid_Current_Squad (Custom).png")
				)
			),		
			array("team_id" => "4", "team_name" => "Club Atletico de Madrid SAD", "stadium" => "Vicente Calderon", "badge" => "atleticomadrid_badge.png", "strips" => "atleticomadrid_strips.png", "city_id" => "2", "dataapi_id" => "78", "info" => "Club Atlético de Madrid, SAD commonly known as Atlético Madrid, or simply as Atlético, is a Spanish professional football club based in Madrid, that plays in La Liga.",
				"images" => array(
					array("image_id" => "73", "description" => "Vicente Calderon", "url" => "VicenteCalderon (Custom).jpg"),
					array("image_id" => "74", "description" => "Atletico Madrid Current Squad", "url" => "AtleticoMadrid_Current_Squad (Custom).png")
				)
			),	
			array("team_id" => "5", "team_name" => "Rayo Vallecano de Madrid SAD", "stadium" => "Vallecas", "badge" => "rayovallecano_badge.png", "strips" => "rayovallecano_strips.png", "city_id" => "2", "dataapi_id" => "87", "info" => "Rayo Vallecano de Madrid, S.A.D., often abbreviated to Rayo, is a Spanish football team based in Madrid, in the neighbourhood of Vallecas. Founded on 29 May 1924, the club currently play in La Liga, Spain's top-flight division.",
				"images" => array(
					array("image_id" => "75", "description" => "Estadio de Vallecas", "url" => "Estadio_de_Vallecas (Custom).jpg"),
					array("image_id" => "76", "description" => "Rayo Vallecano Current Squad", "url" => "RayoVallecano_Current_Squad (Custom).png")	
				)
			),
			array("team_id" => "6", "team_name" => "Athletic Club", "stadium" => "San Mames", "badge" => "athleticbilbao_badge.png", "strips" => "athleticbilbao_strips.png", "city_id" => "3", "dataapi_id" => "77", "info" => "Athletic Club;, also commonly known as Athletic Club de Bilbao;, is a professional football club, based in Bilbao, Basque Country, Spain. They are known as Los Leones because their stadium was built near a church called San Mamés.",
				"images" => array(
					array("image_id" => "77", "description" => "San Mames", "url" => "San Mames (Custom).jpg"),
					array("image_id" => "78", "description" => "Athletic Bilbao Current Squad", "url" => "AthleticBilbao_Current_Squad (Custom).png")	
				)
			),			
			array("team_id" => "7", "team_name" => "Real Betis Balompie SAD", "stadium" => "Benito Villamarin", "badge" => "realbetis_badge.png", "strips" => "realbetis_strips.png", "city_id" => "4", "dataapi_id" => "90", "info" => "Real Betis Balompié, SAD, more commonly referred to as Real Betis or just Betis, is a Spanish football club based in Seville, in the autonomous community of Andalusia.",
				"images" => array(
					array("image_id" => "79", "description" => "Estadio Betis", "url" => "Panorama_Estadio_Betis (Custom).jpg"),
					array("image_id" => "80", "description" => "Real Betis Current Squad", "url" => "RealBetis_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "8", "team_name" => "Sevilla Futbol Club SAD", "stadium" => "Ramon Sanchez-Pizjuan", "badge" => "sevilla_badge.png", "strips" => "sevilla_strips.png", "city_id" => "4", "dataapi_id" => "559", "info" => "Sevilla Fútbol Club, S.A.D., or simply Sevilla, is Spain’s oldest football club solely devoted to association football. Sevilla FC is based in Seville, capital and largest city of the autonomous community of Andalusia.",
				"images" => array(
					array("image_id" => "81", "description" => "Estadio Ramon Sanchez-Pizjuan", "url" => "Estadio_Ramón_Sánchez_Pizjuán (Custom).jpg"),
					array("image_id" => "82", "description" => "Sevilla Current Squad", "url" => "Sevilla_Current_Squad (Custom).png")	
				)
			),
			array("team_id" => "9", "team_name" => "Valencia Club de Futbol SAD", "stadium" => "Mestalla", "badge" => "valencia_badge.png", "strips" => "valencia_strips.png", "city_id" => "5", "dataapi_id" => "95", "info" => "Valencia are a Spanish football club based in Valencia. They play in La Liga and are one of the most successful and biggest clubs in Spanish football and European football.",
				"images" => array(
					array("image_id" => "83", "description" => "Mestalla", "url" => "Mestallapanoramic (Custom).jpg"),
					array("image_id" => "84", "description" => "Valencia Current Squad", "url" => "Valencia_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "10", "team_name" => "Levante Union Deportiva SAD", "stadium" => "Ciutat de Valencia", "badge" => "levante_badge.png", "strips" => "levante_strips.png", "city_id" => "5", "dataapi_id" => "88", "info" => "Levante Unión Deportiva, S.A.D. is a Spanish football club based in Valencia, in the namesake community. Founded on 9 September 1909 it plays in La Liga, holding home games at Ciutat de València.",
				"images" => array(
					array("image_id" => "85", "description" => "Ciutat de Valencia", "url" => "Ciutat_de_valencia (Custom).jpg"),
					array("image_id" => "86", "description" => "Levante Current Squad", "url" => "Levante_Current_Squad (Custom).png")	
				)
			),
			array("team_id" => "11", "team_name" => "Real Club Celta de Vigo SAD", "stadium" => "Balaidos", "badge" => "celtavigo_badge.png", "strips" => "celtavigo_strips.png", "city_id" => "6", "dataapi_id" => "558", "info" => "Real Club Celta de Vigo, or simply Celta Vigo or sometimes just Celta, is a Spanish professional football club based in Vigo, Galicia, currently playing in the Liga.",
				"images" => array(
					array("image_id" => "87", "description" => "Grada Río", "url" => "Grada_Río (Custom).jpg"),
					array("image_id" => "88", "description" => "Celta Vigo Current Squad", "url" => "CeltaVigo_Current_Squad (Custom).png")
				)
			),		
			array("team_id" => "12", "team_name" => "Sociedad Deportiva Eibar SAD", "stadium" => "Ipura", "badge" => "eibar_badge.png", "strips" => "eibar_strips.png", "city_id" => "7", "dataapi_id" => "278", "info" => "Sociedad Deportiva Eibar is a Spanish football club based in Eibar, Gipuzkoa, in the autonomous Basque Country. Founded on 1 January 1940, the team currently plays in La Liga.",
				"images" => array(
					array("image_id" => "89", "description" => "Ipurúa", "url" => "Ipurúa.png"),
					array("image_id" => "90", "description" => "Eibar Current Squad", "url" => "Eibar_Current_Squad (Custom).png")	
				)
			),
			array("team_id" => "13", "team_name" => "Real Club Deportivo de La Coruna, SAD", "stadium" => "Riazor", "badge" => "deportivolacoruna_badge.png", "strips" => "deportivolacoruna_strips.png", "city_id" => "8", "dataapi_id" => "560", "info" => "Real Club Deportivo de La Coruña (English: Royal Sporting Club of La Coruña) is a professional football club based in the city of A Coruña (known in Spanish as La Coruña), Galicia, Spain.",
				"images" => array(
					array("image_id" => "91", "description" => "Estadio de Riazorv", "url" => "Estadio_de_Riazor.A_Corunha.Galiza (Custom).jpg"),
					array("image_id" => "92", "description" => "Deportivo La Coruna Current Squad", "url" => "DeportivoLaCoruna_Current_Squad (Custom).png")
				)
			),		
			array("team_id" => "14", "team_name" => "Granada Club de Futbol SAD", "stadium" => "Los Carmenes", "badge" => "granada_badge.png", "strips" => "granada_strips.png", "city_id" => "9", "dataapi_id" => "83", "info" => "Granada Club de Fútbol, or simply Granada CF, is a Spanish football club based in Granada, in the autonomous community of Andalusia. Founded on 14 April 1931, it currently plays in La Liga, holding home matches at Estadio Nuevo Los Cármenes.",
				"images" => array(
					array("image_id" => "93", "description" => "Estadio Nuevo Los Cármenes", "url" => "Estadio_Nuevo_Los_Cármenes (Custom).jpg"),
					array("image_id" => "94", "description" => "Granada Current Squad", "url" => "Granada_Current_Squad (Custom).png")					
				)
			),				
			array("team_id" => "15", "team_name" => "Union Deportiva Las Palmas SAD", "stadium" => "Gran Canaria", "badge" => "laspalmas_badge.png", "strips" => "laspalmas_strips.png", "city_id" => "10", "dataapi_id" => "275", "info" => "Unión Deportiva Las Palmas, S.A.D. is a Spanish football team based in Las Palmas de Gran Canaria, in the autonomous community of Canary Islands.",
				"images" => array(
					array("image_id" => "95", "description" => "Estadio Gran Canaria", "url" => "Estadio Gran Canaria.jpg"),
					array("image_id" => "96", "description" => "Las Palmas Current Squad", "url" => "LasPalmas_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "16", "team_name" => "Malaga Club de Futbol SAD", "stadium" => "La Rosaleda", "badge" => "malaga_badge.png",  "strips" => "malaga_strips.png", "city_id" => "11", "dataapi_id" => "84", "info" => "Málaga Club de Fútbol, or simply Málaga, is a Spanish football club based in Málaga, Spain. The team currently plays in La Liga, the top division of Spanish football.",
				"images" => array(
					array("image_id" => "97", "description" => "Estadio de la Rosaleda", "url" => "Estado_de_la_Rosaleda (Custom).jpg"),
					array("image_id" => "98", "description" => "Malaga Current Squad", "url" => "Malaga_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "17", "team_name" => "Real Sociedad de Futbol SAD", "stadium" => "Anoeta", "badge" => "realsociedad_badge.png", "strips" => "realsociedad_strips.png", "city_id" => "12", "dataapi_id" => "92", "info" => "Real Sociedad de Fútbol, S.A.D., more commonly referred to as Real Sociedad or La Real, is a Spanish football club based in the city of San Sebastián, Basque Country, founded on 7 September 1909.",
				"images" => array(
					array("image_id" => "99", "description" => "Estadio Anoeta", "url" => "Tribuna_oeste_del_Estadio_Anoeta (Custom).jpg"),
					array("image_id" => "100", "description" => "Real Sociedad Current Squad", "url" => "RealSociedad_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "18", "team_name" => "Real Sporting de Gijon SAD", "stadium" => "El Molinon", "badge" => "sportinggijon_badge.png", "strips" => "sportinggijon_strips.png", "city_id" => "13", "dataapi_id" => "96", "info" => "Real Sporting de Gijón, S.A.D. is a Spanish football club from Gijón, Asturias. Founded on 1 June 1905, it plays in La Liga.",
				"images" => array(
					array("image_id" => "101", "description" => "El Molinó", "url" => "Interior_de_El_Molinón (Custom).jpg"),
					array("image_id" => "102", "description" => "Sporting Gijon Current Squad", "url" => "SportingGijon_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "19", "team_name" => "Villarreal Club de Futbol SAD", "stadium" => "El Madrigal", "badge" => "villarreal_badge.png", "strips" => "villarreal_strips.png", "city_id" => "14", "dataapi_id" => "94", "info" => "Villarreal Club de Fútbol, S.A.D., usually abbreviated to Villarreal CF or just Villarreal, is a Spanish football club based in Vila-real, a city in the province of Castellón within the Valencian Community.",
				"images" => array(
					array("image_id" => "103", "description" => "El Madrigal", "url" => "El_Madrigal (Custom).jpg"),
					array("image_id" => "104", "description" => "Villarreal Current Squad", "url" => "Villarreal_Current_Squad (Custom).png")	
				)
			),	
			array("team_id" => "20", "team_name" => "Getafe Club de Futbol SAD", "stadium" => "Coliseum Alfonso Perez", "badge" => "getafe_badge.png", "strips" => "getafe_strips.png", "city_id" => "15", "dataapi_id" => "82", "info" => "Getafe Club de Fútbol, or simply Getafe CF, is a Spanish professional football club contesting in La Liga. The club is based in Getafe, a city in the Madrid metropolitan area. Getafe was founded in 1946 and refounded in 1983. ",
				"images" => array(
					array("image_id" => "105", "description" => "Coliseum Getafe", "url" => "Coliseum-getafe (Custom).jpg"),
					array("image_id" => "106", "description" => "Getafe Current Squad", "url" => "Getafe_Current_Squad (Custom).png")	
				)
			),			
	);

	echo json_encode($teamsarray);
?>