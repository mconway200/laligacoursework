<?php
	$citiesarray = array(
			array("city_id" => "1", "city_name" => "Barcelona", "region_id" => "1", "lat" => "41.3833", "long" => "2.1833", "info" => "Barcelona, the cosmopolitan capital of Spain’s Catalonia region, is defined by quirky art and architecture, imaginative food and vibrant street life. It has medieval roots, seen in the mazelike Gothic Quarter, but a modernist personality represented by architect Antoni Gaudí’s fantastical Sagrada Família church. Its restaurant scene, anchored by the central Boqueria market, ranges from fine dining to tiny tapas bars.",
				"images" => array(
					array("image_id" => "22", "description" => "Pedrera", "url" => "Pedrera (Custom).jpg"),
					array("image_id" => "23", "description" => "Plaza", "url" => "Plaza (Custom).jpg"),
					array("image_id" => "24", "description" => "Sagrada Familia", "url" => "SagradaFamilia (Custom).jpg")
				)
			),
			array("city_id" => "2", "city_name" => "Madrid", "region_id" => "2", "lat" => "40.4000", "long" => "-3.7167", "info" => "Madrid, Spain's central capital, is a city of elegant boulevards and expansive, manicured parks such as the Buen Retiro. It’s renowned for its rich repositories of European art, including the Prado Museum’s works by Goya, Velázquez and other Spanish masters. The heart of old Hapsburg Madrid is the portico-lined Plaza Mayor, and nearby is the baroque Royal Palace and Armory, displaying historic weaponry.",
				"images" => array(
					array("image_id" => "25", "description" => "Plaza Mayor", "url" => "Plaza Mayor (Custom).jpg"),
					array("image_id" => "26", "description" => "Retiro", "url" => "Retiro (Custom).jpg"),
					array("image_id" => "27", "description" => "Hotel Palacio San Martin)", "url" => "Hotel Palacio San Martin (Custom).jpg")
				)
			),
			array("city_id" => "3", "city_name" => "Bilbao", "region_id" => "3", "lat" => "43.2569", "long" => "-2.9236", "info" => "Bilbao, an industrial port city in northern Spain, is surrounded by green mountains. It’s the de facto capital of Basque Country, with a skyscraper-filled downtown. It’s famed for the Frank Gehry–designed Guggenheim Museum Bilbao, which sparked revitalization when it opened in 1997. The museum houses modern and contemporary works from prominent artists, but it’s the curvy, titanium-clad building that receives the most attention.",
				"images" => array(
					array("image_id" => "28", "description" => "Bilbao Collage", "url" => "Bilbao-collage (Custom).jpg"),
					array("image_id" => "29", "description" => "Guggenheim", "url" => "Guggenheim (Custom).jpg"),
					array("image_id" => "30", "description" => "Puppy", "url" => "Puppy (Custom).jpg")
				)
			),
			array("city_id" => "4", "city_name" => "Sevilla", "region_id" => "4", "lat" => "37.3772", "long" => "-5.9869", "info" => "Seville is the capital of southern Spain’s Andalusia region and a hotbed for flamenco dance, especially in the Triana neighborhood. The city is known for its ornate Alcázar castle complex, built during the Moorish Almohad dynasty, and its 18th-century Plaza de Toros de la Real Maestranza bullring. The massive Gothic cathedral is the site of Christopher Columbus’s tomb and a famous minaret turned belltower, the Giralda.",
				"images" => array(
					array("image_id" => "31", "description" => "Sevilla Collage", "url" => "Collagesevilla (Custom).jpg"),
					array("image_id" => "32", "description" => "Espacio Parasol Sevilla", "url" => "Espacio_Parasol_Sevilla (Custom).jpg"),
					array("image_id" => "33", "description" => "Seville panorama", "url" => "Seville_panorama (Custom).jpg")
				)
			),
			array("city_id" => "5", "city_name" => "Valencia", "region_id" => "5", "lat" => "39.4667", "long" => "-0.3833", "info" => "The port city of Valencia is on Spain’s southeastern Orange Blossom Coast, where the Turia River meets the Mediterranean Sea. It’s known for its City of Arts and Sciences, with futuristic structures including a planetarium, an oceanarium and an interactive museum. Valencia also has several beaches, including some within nearby Albufera park, a wetlands reserve with a lake, walking trails and bird-watching.",
				"images" => array(
					array("image_id" => "34", "description" => "l''hemisferic", "url" => "lhemisferic1 (Custom).jpg"),
					array("image_id" => "35", "description" => "l''umbracle", "url" => "lumbracle2 (Custom).jpg"),
					array("image_id" => "36", "description" => "museu del esciencies principe felipe", "url" => "museudelescienciesprincipefelipe1 (Custom).jpg")
				)
			),
			array("city_id" => "6", "city_name" => "Vigo", "region_id" => "6", "lat" => "42.2314", "long" => "-8.7124", "info" => "Vigo is a city on the Atlantic Ocean in the province of Pontevedra, in Galicia, north-west Spain. Vigo is the most populous municipality in Galicia, and the 14th in Spain. Vigo is in the south-west of Galicia, in the southern part of Vigo Bay.",
				"images" => array(
					array("image_id" => "37", "description" => "Casa do concello de Vigo", "url" => "Casa_do_concello_de_Vigo,_Galiza (Custom).jpg"),
					array("image_id" => "38", "description" => "Pastor Caixa Nova", "url" => "Pastorcaixanova (Custom).jpg"),					
					array("image_id" => "39", "description" => "Rua dos Cesteiros", "url" => "Strato_de_la_korbofaristoj_(Rua_dos_Cesteiros),_Vigo.jpg")				
				)
			),	
			array("city_id" => "7", "city_name" => "Eibar", "region_id" => "3", "lat" => "43.1833", "long" => "-2.4667", "info" => "Eibar is a city and municipality within the province of Gipuzkoa, in the Basque Country of Spain. It is the head town of Debabarrena, one of the comarcas of Gipuzkoa. Eibar has 27,439 inhabitants.",
				"images" => array(
					array("image_id" => "40", "description" => "Eibar", "url" => "Eibar (Custom).jpg"),
					array("image_id" => "41", "description" => "Arrate", "url" => "Arrate (Custom).jpg"),					
					array("image_id" => "42", "description" => "Ayuntamiento Eibar", "url" => "Ayuntamiento_Eibar (Custom).jpg")
				)
			),	
			array("city_id" => "8", "city_name" => "A Coruna", "region_id" => "6", "lat" => "43.3650", "long" => "-8.4100", "info" => "A Coruña is a city and municipality of Galicia, Spain. It is the second-largest city in the autonomous community and seventeenth overall in the country.",
				"images" => array(
					array("image_id" => "43", "description" => "Castillo de San Antón", "url" => "Castillo_de_San_Antón.jpg"),
					array("image_id" => "44", "description" => "Torre de Hércules", "url" => "Torre_de_Hércules_-_DivesGallaecia (Custom).jpg"),					
					array("image_id" => "45", "description" => "A Coruna Collage", "url" => "Collage_de_fotos_coruna (Custom).jpg")				
				)
			),	
			array("city_id" => "9", "city_name" => "Granada", "region_id" => "4", "lat" => "37.1781", "long" => "-3.6008", "info" => "Granada, in the foothills of the Sierra Nevada mountains in Andalucía, has many important examples of medieval architecture dating to the Moorish occupation of Spain. It’s best known for the Alhambra, a grand, sprawling hilltop fortress complex encompassing royal palaces, serene patios and reflecting pools from the Nasrid dynasty, as well as the fountains and orchards of the Generalife gardens.",
				"images" => array(
					array("image_id" => "46", "description" => "Alhambra", "url" => "Alhambra (Custom).jpg"),
					array("image_id" => "47", "description" => "Granada Cathedral Front", "url" => "Granada_-_Cathedral_Front (Custom).jpg"),
					array("image_id" => "48", "description" => "Granada Collage", "url" => "Granada_collage1 (Custom).jpg")					
				)
			),	
			array("city_id" => "10", "city_name" => "Las Palmas de Gran Canaria", "region_id" => "7", "lat" => "28.1500", "long" => "-15.4167", "info" => "Las Palmas is a capital of Gran Canaria, one of Spain's Canary Islands off northwestern Africa. A major cruise-ship port, the city is known for duty-free shopping and for its sandy beaches. At Playa de Las Canteras, a coral barrier lines the beach and shelters swimmers. The annual Las Palmas de Gran Canaria carnival brings together flamboyantly costumed performers, music and dancing.",
				"images" => array(
					array("image_id" => "49", "description" => "Las Palmas de Gran Canaria Collage", "url" => "Collage_Las_Palmas_de_Gran_Canaria.jpg"),
					array("image_id" => "50", "description" => "Columbus House Vegueta", "url" => "Columbus_House-Vegueta-Las_Palmas_Gran_Canaria (Custom).jpg"),
					array("image_id" => "51", "description" => "Lady Harimaguada-Chirino", "url" => "Lady_Harimaguada-Chirino-Las_Palmas_de_Gran_Canaria.jpg")				
				)
			),		
			array("city_id" => "11", "city_name" => "Malaga", "region_id" => "4", "lat" => "36.7194", "long" => "-4.4200", "info" => "Málaga is a port city on southern Spain’s Costa del Sol, known for its high-rise hotels and resorts jutting up from yellow-sand beaches. But looming over that modern skyline are the city’s 2 massive hilltop citadels, the Alcazaba and ruined Gibralfaro, remnants of Moorish rule, along with a soaring Renaissance cathedral, nicknamed La Manquita ('one-armed woman') because one of its towers was curiously left unbuilt.",
				"images" => array(
					array("image_id" => "52", "description" => "Malaga Cathedral", "url" => "Malaga Cathedral (Custom).jpg"),
					array("image_id" => "53", "description" => "Hotel California", "url" => "Hotel California (Custom).jpg"),
					array("image_id" => "54", "description" => "La Malagueta", "url" => "La_Malagueta2 (Custom).jpg")
				)
			),	
			array("city_id" => "12", "city_name" => "San Sebastian", "region_id" => "3", "lat" => "43.3214", "long" => "-1.9856", "info" => "San Sebastián is a resort town on the Bay of Biscay in Spain’s mountainous Basque Country. It’s known for Playa de la Concha and Playa de Ondarreta, beaches framed by a picturesque bayfront promenade, and world-renowned restaurants helmed by innovative chefs. In its cobblestoned old town (Parte Vieja), upscale shops neighbor vibrant pintxo bars pairing local wines with bite-size regional specialties.",
				"images" => array(
					array("image_id" => "55", "description" => "San Sebastian aerial", "url" => "San_Sebastian_aerial (Custom).jpg"),
					array("image_id" => "56", "description" => "Plaza Constitución", "url" => "Plaza_Constitución (Custom).jpg"),
					array("image_id" => "57", "description" => "Puente Maria Cristina, San Sebastian", "url" => "San_Sebastian_Puente_Maria_Cristina (Custom).jpg")				
				)
			),	
			array("city_id" => "13", "city_name" => "Gijon", "region_id" => "8", "lat" => "43.5333", "long" => "-5.7000", "info" => "Gijón, or Xixón is the largest city and municipality in the autonomous community of Asturias in Spain. Early medieval texts mention it as 'Gigia'. It is located on the Bay of Biscay, approximately 20 km north of Oviedo, the capital of Asturias.",
				"images" => array(
					array("image_id" => "58", "description" => "Letronas de Gijon", "url" => "Letronas_de_Gijon (Custom).jpg"),
					array("image_id" => "59", "description" => "Gijon Montage", "url" => "Montaje_Gijon (Custom).jpg"),
					array("image_id" => "60", "description" => "Sede Juan Barjola", "url" => "Sede_Juan_Barjola (Custom).jpg")				
				)
			),	
			array("city_id" => "14", "city_name" => "Villarreal", "region_id" => "5", "lat" => "39.9378", "long" => "-0.1014", "info" => "Villarreal, officially Vila-real, is a city in the province of Castelló, in the Valencian Community, Spain. The city is located at 42 m above sea level, 7 km to the south of the province's capital, which it is separate from by the Mijares River.",
				"images" => array(
					array("image_id" => "61", "description" => "Vila-real Vila Square", "url" => "Vila-real_Vila_Square_10 (Custom).jpg"),
					array("image_id" => "62", "description" => "Martin Vilareal", "url" => "Martin_Vilareal_a (Custom).jpg"),
					array("image_id" => "63", "description" => "Campanar Vilareal", "url" => "Campanar_vila-real (Custom).jpg")				
				)
			),	
			array("city_id" => "15", "city_name" => "Getafe", "region_id" => "2", "lat" => "40.3047", "long" => "-3.7311", "info" => "Getafe is a city in the south of the Madrid metropolitan area, Spain, and one of the most populated and industrialised cities in the area. It is home to one of the oldest Spanish military air bases and the Universidad Carlos III de Madrid.",
				"images" => array(
					array("image_id" => "64", "description" => "Getafe nevado", "url" => "Getafe-nevado.jpg"),
					array("image_id" => "65", "description" => "Manzanares rio", "url" => "Manzanares-rio (Custom).jpg"),
					array("image_id" => "66", "description" => "Cerro Angeles", "url" => "Cerro-angeles (Custom).jpg")					
				)
			),			
	);

	echo json_encode($citiesarray);
?>