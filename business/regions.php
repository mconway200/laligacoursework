<?php
	$regionsarray = array(
			array("region_id" => "1", "region_name" => "Catalonia", "lat" => "41.8167", "long" => "1.4667", "flag" => "Catalonia_Flag.png", "location" => "Catalonia_Location.png", "info" => "The Catalonia region, in northeastern Spain, is known for the lively beach resorts of Costa Brava, the Pyrenees mountains and as home to surrealist artist Salvador Dalí. Barcelona, the regional capital, has an ancient Gothic Quarter, La Rambla pedestrian mall and several beaches. Antoni Gaudí’s distinctive architecture can be seen at the Sagrada Família basilica and in the colorful mosaics of Park Güell.", 
				"images" => array(
					array("image_id" => "1", "description" => "El Greco, Sitges", "url" => "El Greco.jpg"),				
					array("image_id" => "2", "description" => "Pont Eiffel, Girona", "url" => "Pont Eiffel (Custom).jpg"),
					array("image_id" => "3", "description" => "Sitges", "url" => "Sitges (Custom).jpg"),					
				)
			),
			array("region_id" => "2", "region_name" => "Community of Madrid", "lat" => "40.5", "long" => "-3.6667", "flag" => "CommunityOfMadrid_Flag.png", "location" => "CommunityOfMadrid_Location.png", "info" => "The Community of Madrid is one of the seventeen autonomous communities of Spain. It is located at the centre of the country, the Iberian Peninsula, and the Castilian Central Plateau",
				"images" => array(
					array("image_id" => "4", "description" => "Barajas Airport", "url" => "Barajas_T4 (Custom).jpg"),
					array("image_id" => "5", "description" => "Circo y cima de Peñalara1 (Custom)", "url" => "Circo_y_cima_de_Peñalara1 (Custom).jpg"),
					array("image_id" => "6", "description" => "Vista aerea del Monasterio de El Escorial", "url" => "Vista_aerea_del_Monasterio_de_El_Escorial (Custom).jpg")
				)
			),
			array("region_id" => "3", "region_name" => "Basque Country", "lat" => "42.8333", "long" => "-2.6833", "flag" => "BasqueCountry_Flag.png", "location" => "BasqueCountry_Location.png", "info" => "The Basque Country is the name given to the home of the Basque people in the western Pyrenees that spans the border between France and Spain on the Atlantic coast.",
				"images" => array(
					array("image_id" => "7", "description" => "Basque Country Mountains", "url" => "Mountains (Custom).jpg"),				
					array("image_id" => "8", "description" => "Un Sociedad", "url" => "Sociedad (Custom).jpg"),
					array("image_id" => "9", "description" => "Vizcaya Bridge", "url" => "Vizcaya Bridge (Custom).jpg")
				)
			),
			array("region_id" => "4", "region_name" => "Andalusia", "lat" => "37.3833", "long" => "-5.9833", "flag" => "Andalusia_Flag.png", "location" => "Andalusia_Location.png", "info" => "Andalusia, a rocky, sun-baked region on Spain’s southern coast, embodies much of what the world thinks of as Spanish: flamenco, tapas, matadors and bullfights. Yet it was under Moorish rule from the 8th-15th centuries, a legacy that shows in its architecture, including such landmarks as the Alcázar castle in Seville, the capital city, as well as Córdoba’s Mezquita Mosque-Cathedral and Granada’s Alhambra palace.",
				"images" => array(
					array("image_id" => "10", "description" => "Buddha, Benalmadena", "url" => "Buddha (Custom).jpg"),	
					array("image_id" => "11", "description" => "Mosque, Cordoba", "url" => "Mosque_Cordoba (Custom).jpg"),	
					array("image_id" => "12", "description" => "Río Guadalquivir, Cordoba", "url" => "Río_Guadalquivir_Cordoba (Custom).jpg"),
				)
			),
			array("region_id" => "5", "region_name" => "Valencia", "lat" => "39.5", "long" => "-0.75", "flag" => "Valencia_Flag.png", "location" => "Valencia_Location.png", "info" => "The port city of Valencia is on Spain’s southeastern Orange Blossom Coast, where the Turia River meets the Mediterranean Sea. It’s known for its City of Arts and Sciences, with futuristic structures including a planetarium, an oceanarium and an interactive museum. Valencia also has several beaches, including some within nearby Albufera park, a wetlands reserve with a lake, walking trails and bird-watching.",
				"images" => array(
					array("image_id" => "13", "description" => "El Castillo de Santa Barbara", "url" => "Barbara (Custom).jpg"),				
					array("image_id" => "14", "description" => "Puerto de Alicante", "url" => "Puerto_de_Alicante_desde_el_Castillo_de_Santa_Bárbara (Custom).jpg"),
					array("image_id" => "15", "description" => "Castillo de Denia", "url" => "Castillo_de_Denia (Custom).jpg")
				)
			),
			array("region_id" => "6", "region_name" => "Galicia", "lat" => "42.5", "long" => "-8.1", "flag" => "Galicia_Flag.png", "location" => "Galicia_Location.png", "info" => "Galicia, in Spain’s northwest, is a verdant region with an indented Atlantic coastline. The cathedral of regional capital Santiago de Compostela is the reputed burial place of Saint James the Great, and the destination for those following the Camino de Santiago pilgrimage route. The western cliffs of Cape Finisterre were considered by the Romans to be the end of the known world.",
				"images" => array(			
					array("image_id" => "16", "description" => "Aynutamiento de Santiago de Compostela", "url" => "Ayuntamiento_de_Santiago_de_Compostela (Custom).jpg"),
					array("image_id" => "17", "description" => "Carnota", "url" => "Carnota (Custom).jpg")					
				)
			),			
			array("region_id" => "7", "region_name" => "Canary Islands", "lat" => "28.1", "long" => "-15.4", "flag" => "CanaryIslands_Flag.png", "location" => "CanaryIslands_Location.png", "info" => "The Canary Islands, a volcanic archipelago off the coast of northwestern Africa, are among Spain’s farthest-flung territories. These rugged islands are known for their black- and white-sand beaches, varied resorts and balmy climate. Tenerife, the largest island, is dominated by the sometimes-snowy peaks of volcanic Mt. Teide, part of a national park with its own astronomical observatory.",
				"images" => array(
					array("image_id" => "18", "description" => "Mount Teide", "url" => "MountTeide_Tenerife (Custom).jpg"),
					array("image_id" => "19", "description" => "Parque San Telmo, Las Palmas de Gran Canaria", "url" => "Las_palmas_gran_canaria_parque_san_telmo (Custom).jpg")					
				)
			),			
			array("region_id" => "8", "region_name" => "Asturias", "lat" => "43.3333", "long" => "-6", "flag" => "Asturias_Flag.png", "location" => "Asturias_Location.png", "info" => "The Principality of Asturias, in northwest Spain, is known for its rugged coastline, mountain landscapes, religious sites and medieval architecture. Mountaineers, cavers, kayakers and walkers are drawn to the peaks of the Picos de Europa National Park, set above green fields and stone villages. The Covadonga Sanctuary to the Virgin Mary, patron of Asturias, sits in a cave above a waterfall.",
				"images" => array(
					array("image_id" => "20", "description" => "Plaza de Domingo Acebal, Aviles", "url" => "Aviles_-_Plaza_de_Domingo_Acebal (Custom).jpg"),		
					array("image_id" => "21", "description" => "Iglesia de Santa Maria del Naranco", "url" => "Iglesia_de_Santa_María_del_Naranco (Custom).jpg")					
				)
			),				
	);

	echo json_encode($regionsarray);
?>