var laLigaControllers = angular.module("laLigaControllers", ['ui.bootstrap']);

laLigaControllers.controller("HomeController", function ($scope, factory) {

    $scope.isCollapsed = false;
    $scope.myInterval = 3000;
    $scope.noWrapSlides = false;
    $scope.activeSlideTeams = 0;
    $scope.activeSlideCities = 0;


    factory.getTeams().success(function (data) {
        $scope.teamsJSON = data;
    });

    factory.getCities().success(function (data) {
        $scope.citiesJSON = data;
    });

    factory.getApiLeague().success(function (data) {
        $scope.tableJSON = data;
    });

});


laLigaControllers.controller("TeamsController", function ($scope, factory) {

    factory.getTeams().success(function (data) {
        $scope.teamsJSON = data;
    });
});

laLigaControllers.controller("RegionsController", function ($scope, factory) {

    factory.getRegions().success(function (data) {
        $scope.regionsJSON = data;
    });
});

laLigaControllers.controller("CitiesController", function ($scope, factory) {

    factory.getCities().success(function (data) {
        $scope.citiesJSON = data;

        factory.getTeams().success(function (data) {
            $scope.teamsJSON = data;

            $scope.citiesArray = [];
            var teams = [];

            for (var i = 0; i < $scope.citiesJSON.length; i++)
            {
                for (var j = 0; j < $scope.teamsJSON.length; j++)
                {
                    if ($scope.teamsJSON[j].city_id === $scope.citiesJSON[i].city_id)
                    {
                        teams.push(
                                {
                                    index_id: j,
                                    team_name: $scope.teamsJSON[j].team_name
                                });
                    }
                }

                $scope.citiesArray.push(
                        {
                            city_name: $scope.citiesJSON[i].city_name,
                            city_index: $scope.citiesJSON.indexOf($scope.citiesJSON[i]),
                            city_image: $scope.citiesJSON[i].images[0].url,
                            teamsInCity: teams
                        });

                console.log(teams);
                teams = [];
                console.log(teams);
            }
            ;
        });

    });
});

laLigaControllers.controller("TeamDetailsController", function ($scope, factory, $routeParams, uiGmapGoogleMapApi) {

    $scope.whichTeam = $routeParams.team_id;

    uiGmapGoogleMapApi.then(function (maps) {
        console.log("uiGmapGoogleMapApi loaded successful");
    });

    factory.getTeams().success(function (data) {
        $scope.teamsJSON = data;

        var dataApiTeamId = data[$routeParams.team_id].dataapi_id;
        $scope.footballApiUrl = "http://api.football-data.org/v1/teams/" + dataApiTeamId + "/players";

        factory.getApiTeam($scope.footballApiUrl).success(function (data) {
            $scope.teamDataApi = data;
            var playerSize = data.players.length;
            $scope.playerSize = data.players.length;
            $scope.playerSizeSplit = playerSize / 2;
        });

        factory.getCities().success(function (data) {
            $scope.citiesJSON = data;

            for (var i = 0; i < $scope.citiesJSON.length; i++) {
                if ($scope.citiesJSON[i].city_id === $scope.teamsJSON[$scope.whichTeam].city_id) {
                    $scope.teamCity = $scope.citiesJSON[i];
                    $scope.map = {center: {latitude: $scope.teamCity.lat, longitude: $scope.teamCity.long}, cords: {latitude: $scope.teamCity.lat, longitude: $scope.teamCity.long}, zoom: 10};
                }
            }
        });
    });
});

laLigaControllers.controller("CityDetailsController", function ($scope, factory, $routeParams) {

    $scope.myInterval = 3000;
    $scope.whichCity = $routeParams.city_id;

    factory.getCities().success(function (data) {
        $scope.city = data[$routeParams.city_id];

        factory.getRegions().success(function (data) {
            $scope.regionsJSON = data;

            for (var i = 0; i < $scope.regionsJSON.length; i++)
            {
                if ($scope.regionsJSON[i].region_id === $scope.city.region_id)
                {
                    $scope.cityRegion = $scope.regionsJSON[i];
                    $scope.map = {center: {latitude: $scope.city.lat, longitude: $scope.city.long}, cords: {latitude: $scope.city.lat, longitude: $scope.city.long}, zoom: 14};
                }
            }
        });

        factory.getTeams().success(function (data) {
            $scope.teamsJSON = data;

            var teams = [];
            for (var i = 0; i < $scope.teamsJSON.length; i++)
            {
                if ($scope.teamsJSON[i].city_id === $scope.city.city_id)
                {
                    teams.push($scope.teamsJSON[i]);
                }
            }
            $scope.teamsInCity = teams;
            console.log($scope.teamsInCity);
        });
    });
});

laLigaControllers.controller("RegionDetailsController", function ($scope, factory, $routeParams) {

    $scope.myInterval = 3000;

    factory.getRegions().success(function (data) {
        $scope.region = data[$routeParams.region_id];

        factory.getCities().success(function (data) {
            $scope.citiesJSON = data;

            var cities = [];
            for (var i = 0; i < $scope.citiesJSON.length; i++)
            {
                if ($scope.citiesJSON[i].region_id === $scope.region.region_id)
                {
                    cities.push($scope.citiesJSON[i]);
                }
            }
            $scope.citiesInRegion = cities;

            factory.getTeams().success(function (data) {
                $scope.teamsJSON = data;

                var teams = [];
                for (var i = 0; i < $scope.teamsJSON.length; i++)
                {
                    for (var j = 0; j < $scope.citiesInRegion.length; j++)
                    {
                        if ($scope.teamsJSON[i].city_id === $scope.citiesInRegion[j].city_id)
                        {
                            teams.push($scope.teamsJSON[i]);
                        }
                    }
                }
                $scope.teamsInRegion = teams;
            });

        });

        $scope.map = {center: {latitude: $scope.region.lat, longitude: $scope.region.long}, zoom: 7};

    });
});

laLigaControllers.controller("MapController", function ($scope, factory) {

    $scope.map = {center: {latitude: 40.24, longitude: -3.41}, zoom: 5};

    factory.getTeams().success(function (data) {
        $scope.teamsJSON = data;

        factory.getCities().success(function (data) {
            $scope.citiesJSON = data;

            var cityTeamsArray = [];

            var marker = [];
            for (var i = 0; i < $scope.citiesJSON.length; i++)
            {

                var cityJsonIndex = $scope.citiesJSON.indexOf($scope.citiesJSON[i]);

                var cityTeamsArray = [];
                for (var j = 0; j < $scope.teamsJSON.length; j++)
                {
                    if ($scope.teamsJSON[j].city_id === $scope.citiesJSON[i].city_id)
                    {
                        cityTeamsArray.push($scope.teamsJSON[j]);
                    }
                }

                marker.push(
                        {id: i,
                            cords: {latitude: $scope.citiesJSON[i].lat, longitude: $scope.citiesJSON[i].long},
                            window: {title: $scope.citiesJSON[i].city_name},
                            cityIndex: cityJsonIndex,
                            cityTeams: cityTeamsArray
                        });
            }

            $scope.markers = marker;
        });
    });
});
