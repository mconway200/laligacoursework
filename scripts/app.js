var myApp = angular.module("myApp", [
    "ngRoute",
    "ngAnimate",
    "uiGmapgoogle-maps",
    "laLigaControllers",
    "laLigaServices"
]);

myApp.config(["$routeProvider", "uiGmapGoogleMapApiProvider", function ($routeProvider, uiGmapGoogleMapApiProvider) {
        $routeProvider.
                when("/home", {
                    templateUrl: "partials/home.html",
                    controller: "HomeController"
                }).
                when("/teams", {
                    templateUrl: "partials/teams.html",
                    controller: "TeamsController"
                }).
                when("/regions", {
                    templateUrl: "partials/regions.html",
                    controller: "RegionsController"
                }).
                when("/cities", {
                    templateUrl: "partials/cities.html",
                    controller: "CitiesController"
                }).
                when("/map", {
                    templateUrl: "partials/map.html",
                    controller: "MapController"
                }).
                when("/teamdetails/:team_id", {
                    templateUrl: "partials/teamdetails.html",
                    controller: "TeamDetailsController"
                }).
                when("/citydetails/:city_id", {
                    templateUrl: "partials/citydetails.html",
                    controller: "CityDetailsController"
                }).
                when("/regiondetails/:region_id", {
                    templateUrl: "partials/regiondetails.html",
                    controller: "RegionDetailsController"
                }).
                otherwise({
                    redirectTo: "/home"
                });

        uiGmapGoogleMapApiProvider.configure({
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });
    }]);

//myApp.run(function ($rootScope, factory) {
//        $rootScope.citiesJSON = factory.getCities();
//        $rootScope.regionsJSON = factory.getRegions();
//        $rootScope.teamsJSON = factory.getTeams();
//    });
