var laLigaServices = angular.module('laLigaServices', []);


laLigaServices.factory('factory', function ($http) {

    var config = {
        headers: {'X-Auth-Token': '7ff5e1ace1014645b9b536c44a3b8779'}
    };

    return {
        getTeams: function () {
            return $http.get("business/teams.php");
 
        },
        getCities: function () {
            return $http.get("business/cities.php");

        },
        getRegions: function () {
            return $http.get("business/regions.php");
        },
        getApiLeague: function () {
            return $http.get('http://api.football-data.org/v1/soccerseasons/399/leagueTable', config);
        },
        getApiTeam: function (address) {
            return $http.get(address, config);
        }
    };
});

